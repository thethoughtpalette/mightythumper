extends Position2D

var credits_hidden = false

func _process(delta):
	if !credits_hidden && Globals.player.position.x > 2895:
		hide_credits()
		credits_hidden = true

func hide_credits():
	$Stuff/Credits.visible = false