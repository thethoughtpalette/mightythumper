extends CanvasLayer

func _ready():
	$Menu/LevelButton.connect('button_up', self, 'go_to_level', ['IntroLevel'])
	$Menu/LevelButton2.connect('button_up', self, 'go_to_level', ['SweepLevel'])
	$Menu/LevelButton3.connect('button_up', self, 'go_to_level', ['WalkAmongFireballsLevel'])
	$Menu/LevelButton4.connect('button_up', self, 'go_to_level', ['QuickMovesLevel'])
	$Menu/LevelButton5.connect('button_up', self, 'go_to_level', ['LongRangePulseLevel'])
	$MenuButton.connect('button_up', self, 'toggle_menu')
	$Menu/MuteButton.connect('button_up', self, 'toggle_mute')
	$Menu/MuteButton.text = 'Unmute' if Globals.muted else 'Mute'
	
	if not 'IntroLevel' in Globals.enabled_levels:
		$Menu/LevelButton.disabled = true
	if not 'SweepLevel' in Globals.enabled_levels:
		$Menu/LevelButton2.disabled = true
	if not 'WalkAmongFireballsLevel' in Globals.enabled_levels:
		$Menu/LevelButton3.disabled = true
	if not 'QuickMovesLevel' in Globals.enabled_levels:
		$Menu/LevelButton4.disabled = true
	if not 'LongRangePulseLevel' in Globals.enabled_levels:
		$Menu/LevelButton5.disabled = true


func go_to_level(lvl):
	get_tree().get_root().get_node('Root').set_level(lvl)
	
func toggle_menu():
	Globals.menu_is_open = !Globals.menu_is_open
	$AnimationPlayer.play('MenuEnter' if Globals.menu_is_open else 'MenuExit')
	
func toggle_mute():
	Globals.muted = !Globals.muted
	$Menu/MuteButton.text = 'Unmute' if Globals.muted else 'Mute'
	Globals.player.toggle_music()