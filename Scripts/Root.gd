extends Node2D

var current_level

var scene_to_load

func _ready():
	set_level('RobotsWalking')
	
func set_level(scene_name):
	if $AnimationPlayer.is_playing():
		return
	if not scene_name in Globals.enabled_levels:
		Globals.enabled_levels.append(scene_name)

	Globals.has_won = scene_name == 'Meadow'
	
	var scene_path = 'res://Scenes/Levels/{0}.tscn'.format([scene_name])
	scene_to_load = load(scene_path).instance()
	play_transition()
	Globals.menu_is_open = false

func transition_scene ():
	if current_level != null:
		current_level.queue_free()
	current_level = scene_to_load
	scene_to_load.position = Vector2(0, 0)
	add_child(scene_to_load)
	
func play_transition():
	for thumper in $CanvasLayer/Transition.get_children():
		thumper.move()
	$AnimationPlayer.play('Transition')