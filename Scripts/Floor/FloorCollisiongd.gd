extends StaticBody2D

func collided(body=null):
	get_parent().collided(body)
	
func register_object(obj):
	get_parent().register_object(obj)
