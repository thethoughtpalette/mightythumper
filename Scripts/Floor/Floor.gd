extends Position2D

export var tile_width = 15

var tile = preload("res://Scenes/LevelItems/FloorPiece.tscn")
var pulse_obj = load('res://Scripts/Floor/Pulse.gd')

var tiles = []
var tile_linked_items = {}
var pulses = []

var pulse_speed = 600
var max_tile_y_offset = -150

var width = 160

var tree_images = []

var flower = load('res://Art/LevelItems/Flower.png')

func _ready():
	tree_images = [load('res://Art/LevelItems/Tree1.png'), load('res://Art/LevelItems/Tree2.png'), load('res://Art/LevelItems/Tree3.png')]
	width = scale.x * $Sprite.scale.x * tile_width
	$Sprite.modulate.a = 0
	scale.x = 1
	position -= Vector2(cos(rotation), sin(rotation)) * width / 2
	
	$StaticBody2D.position.x += width / 2
	$StaticBody2D/CollisionShape2D.get_shape().set_extents(Vector2(width / 2, 32))
	
	var width_covered = 0
	while (width_covered < width):
		var new_tile = tile.instance()
		new_tile.position.x = width_covered
		add_child(new_tile)
		tiles.append(new_tile)
		
		width_covered += tile_width

	if (rotation == 0 || (rotation_degrees > 178 && rotation_degrees < 182)) && tiles.size() > 30:
		var i = 10
		while i < tiles.size() - 10:
			var x_tree_pos = (i + int(randf() * 50))
			i += 60
			if x_tree_pos < tiles.size() - 10:
				var sprite = Sprite.new()
				sprite.texture = tree_images[int(randf() * 3)]
				sprite.position = Vector2(0, -104)
				tiles[x_tree_pos].add_child(sprite)
				if randf() < 0.5:
					sprite.scale.x *= -1
					
	if Globals.has_won:
		for tile in tiles:
			if randf() < 0.7:
				continue
			var sprite = Sprite.new()
			sprite.position = Vector2(0, -62)
			sprite.texture = flower
			sprite.scale = Vector2(randf() * 0.3 + 0.3, 0.5)
			if randf() < 0.5:
				sprite.scale.x *= -1
			sprite.z_index = 10
			tile.add_child(sprite)
				
		
func _process(delta):
	if pulses.size() == 0:
		return
	
	for tile in tiles:
		tile.position.y = 0
		
	var pulse_index = pulses.size()
	while pulse_index > 0:
		pulse_index -= 1
		pulses[pulse_index].propagate(pulse_speed * delta)
		if pulses[pulse_index].position > width || pulses[pulse_index].position < 0:
			pulses.remove(pulse_index)
		else:
			for i in pulses[pulse_index].get_indexes():
				if i < 0 || i > tiles.size() - 1:
					continue
				tiles[i].position.y = max(max_tile_y_offset, tiles[i].position.y - pulses[pulse_index].query(tiles[i].position.x))
				trigger_connections(tiles[i])
		
		

func collided (colliding_body=null):
	if !colliding_body || !('in_flight' in colliding_body && colliding_body.in_flight):
		return
		
	var t = get_tile_from_global_point(colliding_body.position)
	trigger_connections(t)
	create_pulse(t)
	
	
func get_tile_from_global_point(global_point):
	var closest_point = INF
	var closest_tile = tiles[0]
	for tile in tiles:
		var pos = position + Vector2(cos(rotation), sin(rotation)) * tile.position.x
		var tile_to_point = pos.distance_to(global_point)
		if tile_to_point < closest_point:
			closest_tile = tile
			closest_point = tile_to_point
	return closest_tile
	
func create_pulse(tile):
	var right_pulse = pulse_obj.new(tile.position.x, Globals.RIGHT, tile_width)
	var left_pulse = pulse_obj.new(tile.position.x, Globals.LEFT, tile_width)
	
	pulses.append(right_pulse)
	pulses.append(left_pulse)
	
func register_object(object):
	var t = get_tile_from_global_point(object.position)
	if !tile_linked_items.has(t):
		tile_linked_items[t] = []
	if not object in tile_linked_items[t]:
		tile_linked_items[t].append(object)
		
func trigger_connections(tile):
	if tile_linked_items.has(tile):
			for linked_obj in tile_linked_items[tile]:
				if linked_obj.has_method('floor_damage'):
					linked_obj.floor_damage()