
var position = 0
var direction = Globals.LEFT
var tile_width
var width = 100

var t = 1.1


func _init(initial_pos, dir, tile_w):
	position = initial_pos
	direction = dir
	tile_width = tile_w
	
	t *= 1 if direction == Globals.LEFT else -1
	
func pulse_function2 (x):
	return pow(x / 10, 2)
	
	
func pulse_function (x):
	x = x / 40
	return 65 * pow(4, -pow(x + t, 2))
	
	
func query(x):
	if !within_range(x):
		return 0
	return pulse_function(x - position)
	
func propagate(x):
	position += x if direction == Globals.RIGHT else -x
	
func within_range(x):
	if direction == Globals.LEFT && (x > position || x < position - width):
		false
	if direction == Globals.RIGHT && (x < position || x > position + width):
		false
	return true
	
func get_indexes():
	var current_pos = position
	var indexes = []
	var iteration_addition = -tile_width if direction == Globals.LEFT else tile_width
	while current_pos >= position - width && current_pos <= position + width:
		indexes.append(int(current_pos / tile_width))
		current_pos += iteration_addition
	return indexes