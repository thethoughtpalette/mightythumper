extends Position2D

var thumper = load('res://Scenes/Units/Thumper.tscn')
var started = false

func _ready():
	$MightyThumper.move()
	$Timer.connect('timeout', self, 'play_thumper_anim')
	$AnimationPlayer.connect('animation_finished', self, 'next_scene')

func _process(delta):
	if Input.is_action_just_pressed('scout') && !started:
		started = true
		$Timer.start()
		$Label/PressSpace.visible = false
	if Engine.get_frames_drawn() % 9 == 0:
		spawn_thumper()
	for child in $Thumpers.get_children():
		child.position.x -= 6
		if child.position.x < -2000:
			child.queue_free()
	
func spawn_thumper():
	var thumper_instance = thumper.instance()
	thumper_instance.scale.x *= -1
	thumper_instance.move()
	thumper_instance.z_index = 3 if Engine.get_frames_drawn() % 2 == 0 else 4
	thumper_instance.modulate = Color(0.4 + randf() / 10, 0.4 + randf() / 10, 0.4 + randf() / 10)
	$Thumpers.add_child(thumper_instance)
	
func play_thumper_anim():
	$AnimationPlayer.play('WakeUp')
	
func next_scene(anim_name):
	if anim_name == 'WakeUp':
		$Rumble.stop()
		$AnimationPlayer.play('Onwards')
	elif anim_name == 'Onwards':
		get_tree().get_root().get_node('Root').set_level('IntroLevel')
	
