extends Node

enum DIRECTIONS {UP, RIGHT, DOWN, LEFT}

var floor_tile_pulse_delay = 0.01

var player = null

var emitter = load("res://Scripts/Utils/Emitter.gd").new()

var menu_is_open = false

var muted = false

var has_won = false

var enabled_levels = []