extends KinematicBody2D

var in_flight = false
var flight_speed = 1400
var flight_direction = Vector2(0, 0)
var surface_normal = Vector2(0, -1)

var speed = 500
var dead = false

var win = false
var win_pos = Vector2(0, 0)

var gravity_direction = Vector2(0, 1)
var gravity_magnitude = 600

# (1, 0) moves along the x-axis, (0, 1) moves along the y
var move_direction = Vector2(1, 0)

var respawn_position
var respawn_gravity_direction
var respawn_surface_normal
var respawn_rotation
var respawn_move_direction

var pulse_sound
var wheel_sound

var scouting = false
var camera_scout_speed = 30
var camera_scout_range_limit = 4000

func _ready():
	respawn_position = position
	respawn_gravity_direction = gravity_direction
	respawn_surface_normal = surface_normal
	respawn_rotation = rotation
	respawn_move_direction = move_direction
	
	$DeathTimer.connect('timeout', self, 'revive')
	
	pulse_sound = $PulseSound
	wheel_sound = $WheelSound
	
	Globals.player = self


func _process(delta):
	if dead || win:
		return
		
	if player_outside_boundary():
		kill_player()
		
	
	
	if in_flight:
		var collision = move_and_collide(flight_direction * flight_speed * delta)
		if collision:
			if collision.collider.has_method('collided'):
				collision.collider.collided(self)
				
			if dead:
				return
				
			in_flight = false
			rotation = atan2(collision.normal.x, -collision.normal.y)
			surface_normal = collision.normal
			$CameraNode/Camera2D.shake()
			if (!pulse_sound.playing || pulse_sound.get_playback_position() > 0.2) && !Globals.muted:
				pulse_sound.play()
			wheel_sound.stop()
			$Thumper.idle()
			move_direction = collision.normal.tangent().abs()
			move_and_collide(move_direction * speed * delta)
			gravity_direction = -collision.normal.normalized()
	else:
		gravity_movement(delta)
		
		if Input.is_action_just_pressed('scout'):
			scouting = true
			$Thumper.scout()
		if Input.is_action_just_released("scout"):
			$Thumper.unscout()
			$CameraNode.position = Vector2(0, 0)
			scouting = false
			
		var col = null
		if Input.is_action_pressed("ui_left") || Input.is_action_pressed("ui_up"):
			if scouting:
				var movement = Vector2(cos(rotation), -sin(rotation)) * camera_scout_speed
				$CameraNode.position -= movement
			else:
				col = move_and_collide(move_direction * -speed * delta)
				$Thumper.scale.x = abs($Thumper.scale.x) if (rotation < 0 && rotation >= -PI) else -abs($Thumper.scale.x)
				$Thumper.move()
				play_wheel_sound()
		elif Input.is_action_pressed("ui_right") || Input.is_action_pressed("ui_down"):
			if scouting:
				var movement = Vector2(cos(rotation), -sin(rotation)) * camera_scout_speed
				$CameraNode.position += Vector2(cos(rotation), -sin(rotation)) * camera_scout_speed
			else:
				col = move_and_collide(move_direction * speed * delta)
				$Thumper.scale.x = -abs($Thumper.scale.x) if (rotation < 0 && rotation >= -PI) else abs($Thumper.scale.x)
				$Thumper.move()
				play_wheel_sound()
		else:
			$Thumper.idle()
			wheel_sound.playing = false
			
		if col && col.collider.has_method('collided'):
			col.collider.collided(self)
			
func gravity_movement(delta):
	var gravity_col = move_and_collide(gravity_direction * gravity_magnitude * delta)
	if gravity_col && gravity_col.collider.has_method('collided'):
		gravity_col.collider.collided(self)

func _input(event):
	if dead:
		return
		
   # Mouse in viewport coordinates
	if !scouting && !in_flight && event is InputEventMouseButton && event.button_index == BUTTON_LEFT and !event.is_pressed():
		if (event.position.y < 50 && event.position.x > 660) || Globals.menu_is_open:
			return
		in_flight = true
		$Thumper.fly()
		wheel_sound.stop()
		set_flight_direction(get_global_mouse_position() - position)
		
# expects a vector
func set_flight_direction(dir):
	var clamped_angle = clamp(dir.angle_to(surface_normal), -PI / 2, PI / 2)
	dir = surface_normal.rotated(-clamped_angle)
	flight_direction = dir.normalized()
	
func kill_player():
	if dead || win:
		return
	if !Globals.muted:
		$DeathSound.play()
	position.y -= 680
	in_flight = false
	dead = true
	modulate.a = 0
	$DeathTimer.start()

func revive():
	position = respawn_position
	gravity_direction = respawn_gravity_direction
	surface_normal = respawn_surface_normal
	rotation = respawn_rotation
	move_direction = respawn_move_direction
	modulate.a = 1
	dead = false
	
	Globals.emitter.emit('player_respawn')
	
func player_outside_boundary():
	return position.y < -10 || position.y > 750

func play_wheel_sound():
	if !wheel_sound.playing && !Globals.muted:
		wheel_sound.play(randf() * 5)
	
func set_respawn_pos(pos):
	if pos.x <= respawn_position.x:
		return
	respawn_position = pos
	
func player_win():
	win = true
	
func toggle_music():
	$BackgroundMusic.playing = !Globals.muted