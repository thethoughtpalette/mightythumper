extends Area2D

var fire_ball = preload('res://Scenes/LevelItems/FireBall.tscn')

export var fire_interval = 2.0
export var initial_delay = 0.1
export var disabled_interval = 5.0
export var initial_head_rotation = -90.0

export var sweep = false
export var min_sweep_angle = 0.0
export var max_sweep_angle = 0.0
export var sweep_time = 3.0


var head
var original_head_pos
var original_min_sweep_angle
var original_max_sweep_angle

var tween
var sweep_tween
var disabled_tween
var shock_timer

var smoke
var lightning
var anim
var progress_bar

var disabled = true
var can_shock = true

var fire_balls = []

var fire_range = 4000

func _ready():
	head = $Art/Head
	head.rotation = deg2rad(initial_head_rotation)
	original_head_pos = head.position
	lightning = $Lightning
	smoke = $Smoke
	sweep_tween = $SweepTween
	progress_bar = $Art/Body/ProgressBar
	anim = $AnimationPlayer
	disabled_tween = $DisabledTween
	shock_timer = $ShockTimer
	tween = $Tween
	
	Globals.emitter.connect('player_respawn', self, 'restart')
	
	# connect('area_entered', self, 'shock')
	tween.connect('tween_completed', self, 'fire')
	disabled_tween.connect('tween_completed', self, 'power_on')
	shock_timer.connect('timeout', self, 'make_shockable')
	
	if initial_delay == 0:
		initial_delay = 0.001
	
	connect('body_entered', self, 'register_self')
	
	min_sweep_angle = deg2rad(min_sweep_angle)
	original_min_sweep_angle = min_sweep_angle
	
	max_sweep_angle = deg2rad(max_sweep_angle)
	original_max_sweep_angle = max_sweep_angle
	
	sweep_tween.connect('tween_completed', self, 'turret_sweep')
		
	restart()

func register_self(body):
	disconnect('body_entered', self, 'register_self')
	if body.has_method('register_object'):
		body.register_object(self)
		
func _physics_process(delta):
	for fb in fire_balls:
		var collision = fb.move_and_collide(fb.direction * fb.speed * delta)
		if collision:
			if collision.collider.has_method('kill_player'):
				collision.collider.kill_player()
			fb.collided()
			
func restart():
	disabled = false
	can_shock = true
	smoke.emitting = false
	lightning.emitting = false
	head.position = original_head_pos
	if sweep:
		min_sweep_angle = original_min_sweep_angle
		max_sweep_angle = original_max_sweep_angle
		head.rotation = min_sweep_angle
		turret_sweep()
	shock_timer.stop()
	sweep_tween.stop_all()
	tween.stop_all()
	disabled_tween.stop_all()
	
	disabled_tween.interpolate_property(progress_bar, "value",
                0, 100, initial_delay, Tween.TRANS_LINEAR, Tween.TRANS_LINEAR)
	disabled_tween.start()


func floor_damage():
	shock()

func shock(area=null):
	if can_shock:
		can_shock = false
		shock_timer.start()
		disabled = true
		tween.stop_all()
		sweep_tween.stop_all()
		head.position = original_head_pos
		smoke.emitting = true
		lightning.emitting = true
		anim.play('Shock')
		disabled_tween.stop_all()
		disabled_tween.interpolate_property(progress_bar, "value",
                0, 100, disabled_interval, Tween.TRANS_LINEAR, Tween.TRANS_LINEAR)
		disabled_tween.start()
		
	
func power_on(obj = null, key = null):
	disabled = false
	smoke.emitting = false
	lightning.emitting = false
	if sweep:
		min_sweep_angle = original_min_sweep_angle
		max_sweep_angle = original_max_sweep_angle
		turret_sweep()
	fire()

func fire(obj = null, key = null):
	var dir = Vector2(cos(head.rotation), sin(head.rotation))
	
	head.position -= dir * 100
	tween.interpolate_property(head, "position",
                head.position, original_head_pos, fire_interval,
                Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()
	
	if abs(Globals.player.position.x - position.x) > fire_range:
		return
	
	spawn_fire_ball(Vector2(cos(head.rotation + rotation), sin(head.rotation + rotation)))
	

func spawn_fire_ball (dir=Vector2(1, 0)):
	var fb = fire_ball.instance()
	fb.parent = self
	fire_balls.append(fb)
	fb.position = position
	fb.direction = dir
	fb.get_node('Pivot').rotation = head.rotation + rotation
	get_parent().add_child(fb)


func make_shockable():
	can_shock = true
	
func turret_sweep(obj = null, key = null):
	sweep_tween.stop_all()
	sweep_tween.interpolate_property(head, 'rotation',
                min_sweep_angle, max_sweep_angle, sweep_time,
			    Tween.TRANS_LINEAR, Tween.TRANS_LINEAR)
	var temp = min_sweep_angle
	min_sweep_angle = max_sweep_angle
	max_sweep_angle = temp
	sweep_tween.start()
	