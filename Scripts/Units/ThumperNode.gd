extends Position2D

enum MODE {IDLE, FLY, MOVE}
var animation_mode = -1

func _ready():
	$Tween.connect('tween_completed',self, 'hideBinoculars')
	stop_all()
	idle()
	animation_mode = MODE.IDLE

func idle():
	if animation_mode != MODE.IDLE:
		stop_all()
		animation_mode = MODE.IDLE
		$HeadBodyAnimation.play('Shake')
		$TireAnimation.play('Idle')
	
func move():
	if animation_mode != MODE.MOVE:
		stop_all()
		animation_mode = MODE.MOVE
		$HeadBodyAnimation.play('Shake')
		$TireAnimation.play('Go')

	
func fly():
	if animation_mode != MODE.FLY:
		stop_all()
		animation_mode = MODE.FLY
		$HeadBodyAnimation.play('Fly')
		
func scout():
	$BinocularAnimation.stop()
	$BinocularAnimation.play('Scout', -2)
func unscout():
	$BinocularAnimation.stop()
	$BinocularAnimation.play_backwards('Scout', -2)
	$Tween.stop_all()
	$Tween.start()
func hideBinoculars():
	$Thumper/Head/Stick3.hide()
	
func stop_all():
	$TireAnimation.stop()
	$HeadBodyAnimation.stop()
