extends KinematicBody2D

var direction = Vector2(0, 0)
var speed = 400
var parent

func collided(colliding_body=null):
	if colliding_body != null && colliding_body.has_method('kill_player'):
		colliding_body.kill_player()
	parent.fire_balls.erase(self)
	queue_free()
