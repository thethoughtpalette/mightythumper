extends Position2D

func _ready():
	$Area2D.connect('body_entered', self, 'set_respawn')

func set_respawn (colliding_body):
	if colliding_body.has_method('set_respawn_pos'):
		colliding_body.set_respawn_pos(position)
