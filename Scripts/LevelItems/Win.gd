extends Position2D

export var next_level_name = ""

func _ready():
	$Area2D.connect('body_entered', self, 'player_win')
	$Timer.connect('timeout', self, 'advance_level')


func player_win (colliding_object):
	if colliding_object.has_method('player_win'):
		colliding_object.player_win()
		$Art/Particles2D.restart()
		$Timer.start()
		
func advance_level():
	get_tree().get_root().get_node('Root').set_level(next_level_name)