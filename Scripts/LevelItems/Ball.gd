extends KinematicBody2D

var direction = Vector2(0, 150)
var speed = 2200

var freeze_timer

var collided_wall = null
var no_move = false

var normalized_normal = Vector2(0, 0)

func _ready():
	$MightyThumper.get_node('HeadBodyAnimation').play('Fly')
	freeze_timer = Timer.new()
	freeze_timer.wait_time = 0.15
	freeze_timer.one_shot = true
	freeze_timer.connect('timeout', self, 'collide_with_wall')
	add_child(freeze_timer)
	get_parent().modulate.a = 1

func _physics_process(delta):
	if no_move:
		return
	var velocity = direction * speed
	var collision = move_and_collide(velocity * delta)
	if collision:
		normalized_normal = collision.normal.normalized()
		var motion = collision.remainder.bounce(normalized_normal)
		direction = velocity.bounce(collision.normal)
		if collision.collider.has_method('collided'):
			collided_wall = collision.collider
			collide_with_wall ()
			get_parent().modulate.a = 1

func collide_with_wall ():
	if collided_wall != null && collided_wall.has_method('collided'):
		collided_wall.collided()
		no_move = false
		get_parent().get_node('PlayerBody/Camera2D').shake()
		get_parent().local_gravity = -normalized_normal
		get_parent().position = position
		var player_rotation = atan2(normalized_normal.x, -normalized_normal.y)
		get_parent().rotation = player_rotation
		queue_free()
		

func set_direction (dir):
	direction = dir